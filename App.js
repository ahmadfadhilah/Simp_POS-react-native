import React from 'react';
import AppRouter from './src/routers/AppRouter';
import NoConnections from './src/screens/NoConnections';

const App = () => {
  return <NoConnections/>;
};

export default App;
